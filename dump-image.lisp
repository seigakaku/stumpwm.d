#|
exec sbcl --noinform --load $0 --end-toplevel-options "$@"
|#

(let ((userinit (sb-impl::userinit-pathname)))
  (if (probe-file userinit)
      (load userinit)
    (error "Can't load user init file")))

(user-homedir-pathname )

(asdf:load-asd (uiop:merge-pathnames* ".stumpwm.d/stumpwm-executable.asd" (user-homedir-pathname)))

#+quicklisp
(ql:quickload :stumpwm-executable)

(asdf:make :stumpwm-executable)
