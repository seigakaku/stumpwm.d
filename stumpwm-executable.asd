;;;; -*- Mode: Lisp; -*- vim: set ft=lisp:

#+sb-core-compression
(defmethod perform ((o image-op) (c system))
  (dump-image (output-file o c) :executable t :compression nil))

(defsystem #:stumpwm-executable
  :author "Seiga Kaku <iosevka AT naver DOT com>"
  :depends-on (:stumpwm :serapeum :slynk :machine-state)
  :components ((:static-file "init.lisp")
               (:static-file "dump-image.lisp")
               (:module "src"
                :components ((:static-file "x11.lisp")
                             (:static-file "modeline.lisp")
                             (:static-file "wallpapers.lisp")
                             (:static-file "bindings.lisp")
                             (:static-file "brightness.lisp"))))
  :build-operation "program-op"
  :build-pathname "stumpwm"
  :entry-point "stumpwm::stumpwm")
