(in-package #:stumpwm-user)

(defparameter *backlight*
  (switch-hosts
    ("ideapad" #P"/sys/class/backlight/amdgpu_bl0/")))

(define-stumpwm-enum-type :brightness-operation
    (:up :down :set))

(defun %brightness (&optional (brightness-file (uiop:merge-pathnames* "brightness" *backlight*)))
  (uiop:read-file-form brightness-file))

(defun (setf %brightness) (value &optional (brightness-file (uiop:merge-pathnames* "brightness" *backlight*)))
  (with-open-file (stream brightness-file :direction :output :if-exists :supersede)
    (write value :stream stream)))

(defcommand brightness (operation value)
    ((:brightness-operation "Operation: ")
     (:number "Value: "))
  (let* ((current (%brightness (uiop:merge-pathnames* "brightness" *backlight*)))
         (max (%brightness (uiop:merge-pathnames* "max_brightness" *backlight*)))
         (new (case operation
                (:down (max 0 (- current value)))
                (:up (min max (+ current value)))
                (:set (min (max 0 value) max)))))
    (handler-case
        (progn
          (setf (%brightness) new)
          (message "Brightness: ~d -> ~d" current new))
      (error (e)
        (message "^(:fg red)Failed^* setting brightness from ~d to ~d: ~a" current new e)))))

(define-keys *top-map*
  "XF86MonBrightnessUp" "brightness up 10"
  "XF86MonBrightnessDown" "brightness down 10")
