(in-package #:stumpwm-user)

(defun xinput-device-id-from-name (name)
  (mapcan
   (lambda (l)
     (let ((dev-name (string-trim '(#\Space) (car (uiop:split-string l :separator '(#\Tab))))))
       (when (string= name (subseq dev-name (position-if #'alpha-char-p dev-name)))
         (parse-integer (subseq (find "id=" (uiop:split-string l) :test #'uiop:string-prefix-p) 3)))))
   (uiop:run-program "xinput" :output :lines)))

(defun xinput-property-id-from-name (device name)
  (loop
    for line in (uiop:run-program (u:fmt "xinput list-props '~d'" device) :output :lines)
    when (u:string-contains-p name line)
      do (loop
           for field in (uiop:split-string line :separator '(#\Space #\Colon))
           when (uiop:string-enclosed-p "(" field ")")
             do (return-from xinput-property-id-from-name
                  (subseq field 1 (1- (length field)))))))

(defun (setf xinput-property) (value device prop)
  (uiop:run-program
   (u:fmt "xinput set-prop '~d' '~d' '~d'"
          device
          (etypecase prop
            (number prop)
            (string (xinput-property-id-from-name device prop)))
          (cond
            ((eq value 't) 1)
            ((eq value 'nil) 0)
            (t value)))))

(defun pgrep (proc)
  (zerop (nth-value 2 (uiop:run-program (u:fmt "pgrep ~a" proc) :ignore-error-status t))))

(defun xrandr-monitors ()
  (mapcar (lambda (line)
            (car (uiop:split-string line :separator '(#\Space))))
          (remove-if (lambda (line)
                       (u:whitespacep (u:first-elt line)))
                     (cdr (uiop:run-program "xrandr" :output :lines)))))

(defun xrandr-monitor-present-p (&rest names)
  (dolist (name names)
    (u:when-let ((name (find name (xrandr-monitors) :test #'cl-ppcre:scan)))
      (return-from xrandr-monitor-present-p name))))

(defun xrandr-provider-count ()
  (parse-integer (u:lastcar (uiop:split-string (car (uiop:run-program "xrandr --listproviders" :output :lines))))))

(set-font "-xos4-terminus-medium-r-normal-*-20-*-*-*-*-*-*-*")
(setf (getenv "XDG_SESSION_TYPE") "x11")

(when (u:resolve-executable "xset")
  (uiop:run-program "xset r rate 280 120"))

(switch-hosts
  ("ideapad"
   (when (u:resolve-executable "xrandr")
     (u:when-let ((mon (xrandr-monitor-present-p "HDMI")))
       (uiop:run-program "xrandr --output HDMI-A-0 --preferred --pos 0x0 --primary")
       (uiop:run-program "xrandr --output eDP --preferred --pos 1920x500"))) ;; eDP always present (hopefully)

   (alexandria:when-let ((touchpad-id (xinput-device-id-from-name "Elan Touchpad")))
     (psetf (xinput-property touchpad-id "Tapping") t
            (xinput-property touchpad-id "Drag") t
            (xinput-property touchpad-id "Natural Scrolling") t
            (xinput-property touchpad-id "Disable While Typing") t
            (xinput-property touchpad-id "Middle Emulation") t
            (xinput-property touchpad-id "Horizontal Scroll") t
            (xinput-property touchpad-id "High Resolution Wheel Scroll") t
            (xinput-property touchpad-id "Left Handed") nil)))

  ("desktop"

   (let ((count (xrandr-provider-count)))
     (when (> count 1)
       (uiop:run-program (u:fmt "xrandr --setprovideroutputsource ~{~d~^ ~}" (nreverse (u:iota count))))))

   (uiop:run-program
    (u:fmt "xrandr --output ~a --preferred --pos 0x0"
           (or (xrandr-monitor-present-p "HDMI-1\.*")
               (xrandr-monitor-present-p "DP\.*"))))

   ;; Main monitor
   ( u:when-let ((mon (xrandr-monitor-present-p "HDMI-0*")))
     (uiop:run-program (u:fmt "xrandr --output ~a --preferred --pos 1920x0 --primary" mon)))

   (u:when-let ((mon (xrandr-monitor-present-p "DP-1-1")))
     (uiop:run-program (u:fmt "xrandr --output ~a --off" mon))
     (uiop:run-program (u:fmt "xrandr --output ~a --mode 1600x900 --pos 3840x0" mon)))

   (when (u:resolve-executable "setxkbmap")
     (uiop:run-program "setxkbmap seiga"))))

(defcommand setup-x11 () ()
  (load (swm-path "src/x11.lisp")))
