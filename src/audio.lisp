(in-package #:stumpwm-user)

;;;; Audio control =========================================

;; Amixer ==================
(when (u:resolve-executable "amixer")
  (define-stumpwm-enum-type :audio-operation
      (:decrease :increase :toggle :mute :unmute))

  (defun volume (&optional (sink "Master"))
    (parse-integer
     (second
      (uiop:split-string
       (find "dB"
             (uiop:run-program (u:fmt "amixer sget '~a'" sink)
                               :output :lines)
             :from-end t
             :test #'u:string-contains-p)
       :separator '(#\[ #\])))
     :junk-allowed t))

  (defun (setf volume) (volume &optional (sink "Master"))
    (uiop:run-program (u:fmt "amixer sset '~a' '~d%'" sink volume))
    volume)

  (defun mutedp (&optional (sink "Master"))
    (u:string-contains-p
     "[off]"
     (find "dB" (uiop:run-program (u:fmt "amixer sget '~a'" sink)
                                  :output :lines)
           :from-end t
           :test #'u:string-contains-p)))

  (defun (setf mutedp) (mutedp &optional (sink "Master"))
    (uiop:run-program (u:fmt "amixer set '~a' ~:[unmute~;mute~]" sink mutedp))
    mutedp)

  (defun audio-status (sink start-volume start-mutedp)
    (let ((mutedp (mutedp sink)) (volume (volume sink)))
      (message
       (u:fmt "Volume: ~d -> ~d ~:[~@[[MUTED]~]~;~*(~:[UNMUTED~;MUTED~] -> ~:[UNMUTED~;MUTED~])~]"
              start-volume volume
              (not (eq start-mutedp mutedp))
              mutedp
              start-mutedp mutedp))))


  (defcommand audio-control (operation &key (percentage 10) (sink "Master"))
      ((:audio-operation "Operation?: "))
    (let ((mutedp (mutedp sink)) (volume (volume sink)))
      (case operation
        (:decrease (setf (volume sink) (max 0 (- volume percentage))))
        (:increase (setf (volume sink) (min 100 (+ volume percentage))))
        (:toggle (setf (mutedp sink) (not mutedp)))
        (:mute (setf (mutedp sink) t))
        (:unmute (setf (mutedp sink) nil)))
      (audio-status sink volume mutedp))))

(define-prefix-map *top-map* "s-a"
  "+" "audio-control increase"
  "-" "audio-control decrease")

(define-keys *top-map*
  "XF86AudioMute" "audio-control toggle"
  "XF86AudioLowerVolume" "audio-control decrease"
  "XF86AudioRaiseVolume" "audio-control increase")

;;;; Media Control =====================================

;; Playerctl ==================
(when (u:resolve-executable "playerctl")
  (define-stumpwm-enum-type :media-operation
      (:play-pause :play-pause-all :play-all :pause-all :next :prev))

  (defun media-status (operation)
    (case operation
      (:play-pause "playerctl play-pause")
      (:play "playerctl play")
      (:pause "playerctl -a pause")
      (:next "playerctl -a next")
      (:prev "playerctl -a previous")))

  (defun current-mpris-client ()
    (car (split-string (stripln (run-shell-command "playerctl -l" t)) :separator '(#\Newline))))

  (defun media-status (operation client)
    (apply #'message
           (case operation
             (:play-pause `("Toggling ~a." ,client))
             (:play `("Playing ~a." ,client))
             (:pause "Pausing all MPRIS clients.")
             (:next "Forwarding all MPRIS clients")
             (:prev "Backwarding all MPRIS clients."))))

  (defcommand media-control (operation) ((:media-operation "Operation?: "))
    (let ((client (current-mpris-client)))
      (run-shell-command
       (case operation
         (:play-pause "playerctl play-pause")
         (:play-pause-all "playerctl -a play-pause")
         (:play "playerctl play")
         (:play-all "playerctl -a play")
         (:pause "playerctl pause")
         (:pause-all "playerctl -a pause")
         (:next "playerctl -a next")
         (:prev "playerctl -a previous")))
      (media-status operation client)))

  (define-keys *top-map*
    "XF86AudioPlay" "media-control play-pause-all"
    "XF86AudioPrev" "media-control next"
    "XF86AudioNext" "media-control prev"))
