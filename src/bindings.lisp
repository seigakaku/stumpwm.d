(set-prefix-key (kbd "C-t"))

(define-keys *top-map*
  "s-M-q" "quit"
  "s-M-R" "restart-hard"
  "s-M-r" "reload"
  "s-m" "mode-line")

;; Window control

(defcommand toggle-float-this () ()
  (let ((window (current-window)))
    (if (typep window 'stumpwm::float-window)
        (unfloat-this)
        (float-this))))

(define-keys *top-map*
  "s-n" "next-in-frame"
  "M-Tab" "next"
  "s-p" "prev-in-frame"
  "M-ISO_Left_Tab" "prev"
  "s-0" "select-window-by-number"
  "s-x" "delete-window"
  "s-M-x" "kill-window"
  "s-space" "toggle-float-this")

;; Frame control
(define-keys *top-map*
  "s-|" "hsplit"
  "s--" "vsplit"
  "s-N" "fnext"
  "s-P" "fprev"
  "s-b" "balance-frames"
  "s-S" "fselect"
  "s-X" "remove-split"
  "s-O" "only"
  "s-h" "move-focus left"
  "s-j" "move-focus down"
  "s-k" "move-focus up"
  "s-l" "move-focus right"
  "s-H" "move-window left"
  "s-J" "move-window down"
  "s-K" "move-window up"
  "s-L" "move-window right")

;; Group control
(define-keys *top-map*
  "s-N" "gnext"
  "s-P" "gprev"
  "s-g" "gselect"
  "s-G" "gmove"
  "s-M-g" "gmove-and-follow")

(defparameter number-row-ksyms
  '(#x2b #x7b #x3c #x3e #x26 #x3d #x5e #x40 #x7d))

(defun mkksym (ksym &rest mods &key &allow-other-keys)
  (apply #'stumpwm::make-key :keysym ksym mods))

(loop
  for i from 2 for ksym in number-row-ksyms
  do (define-key *top-map* (mkksym ksym :control t) (u:fmt "gselect ~d" i))
     (define-key *top-map* (mkksym ksym :meta t) (u:fmt "gmove ~d" i))
     (define-key *top-map* (mkksym ksym :control t :meta t) (u:fmt "gmove-and-follow ~d" i)))

(define-key *top-map* (mkksym #x2a :control t) "gselect dyn")
(define-key *top-map* (mkksym #x2a :meta t) "gmove dyn")
(define-key *top-map* (mkksym #x2a :control t :meta t) "gmove-and-follow dyn")

(defcommand screenshot () ()
  (cond
    ((and (u:resolve-executable "scrot") (u:resolve-executable "xclip"))
     (uiop:run-program "scrot -s -ofF /tmp/screenshot.png -e 'xclip -sel c -t image/png $f'"))
    ((u:resolve-executable "flameshot") (uiop:run-program "flameshot gui"))))

(define-keys *top-map*
  "C-|" "rotate-windows forward"
  "C-!" "rotate-windows backward")

;; Programs
(define-keys *top-map*
  "s-E" "exec emacs"
  "s-e" "exec emacsclient -a emacs -c"
  "s-t" "exec alacritty -e tmux"
  "s-r" "exec"
  "s-f" "exec qutebrowser"
  "s-F" "exec firefox -ProfileManager"
  "s-M-p" "screenshot"
  "Print" "screenshot"
  "s-:" "eval")

;; Others
(define-keys *top-map*
  "s-w" "next-wallpapers")
