(in-package #:stumpwm-user)

(uiop:add-package-local-nickname :ms :org.shirakumo.machine-state)

(defun si-format (n)
  (u:format-file-size-human-readable nil n :flavor :iec :space t))

(defun memory-usage ()
  "Return three values used bytes, free bytes and used percentage"
  (u:mvlet ((used total (ms:machine-room)))
    (values used (- total used) (/ used total))))

(defun modeline-memory (modeline)
  (declare (ignore modeline))
  (u:mvlet ((used free mem% (memory-usage)))
    (declare (ignore free))
    (u:fmt "Mem: ~a ~,2f%" (si-format used) (* mem% 100))))
(add-screen-mode-line-formatter #\M 'modeline-memory)

(let* ((paths (list #P"/"))
       (devices (mapcar #'ms:storage-device paths)))
  (defun modeline-disk (modeline)
    (declare (ignore modeline))
    (u:string-join
     (mapcar (lambda (path device)
               (u:mvlet ((used total (ms:storage-room device)))
                 (u:fmt "^(:fg 3)~a^* ^(:fg 3)~,2f / ~,2f^* ^(:fg 2)~,2f%^*"
                        path (si-format used) (si-format total) (* (/ used total) 100))))
             paths devices)
     #\Space)))
(add-screen-mode-line-formatter #\D 'modeline-disk)

(let ((devices (switch-hosts ("desktop" (list "enp3s0" "wg0")))))
  (defun modeline-net (modeline)
    (declare (ignore modeline))
    (u:string-join
     (mapcar (lambda (device)
               (u:mvlet ((total read write (ms:network-io-bytes device)))
                 (declare (ignore total))
                 (u:fmt "^(:fg 2)~a^*: ^(:fg 3)RW^* ~d / ~d"
                        device (si-format read) (si-format write))))
             devices)
     #\Space)))
(add-screen-mode-line-formatter #\N 'modeline-net)

(defun aggregate-cpu-time ()
  (list (get-internal-real-time) (ms:machine-time t)))

(let ((ncpus (u:count-cpus :online t :memoize t))
      (last-cpu (aggregate-cpu-time)))
  (defun cpu-usage (&optional (cpu (aggregate-cpu-time)))
    (destructuring-bind (time0 idle0) last-cpu
      (destructuring-bind (time1 idle1) cpu
        (let ((elapsed-time (/ (- time1 time0) internal-time-units-per-second)))
          (when (>= elapsed-time 1)
            (prog1
                (* 100
                   (- 1.0d0
                      (/ (- idle1 idle0)
                         elapsed-time
                         ncpus)))
              (setf last-cpu cpu)))))))

  (defun cpu-frequency-hz ()
    (let* ((lines (uiop:read-file-lines #P"/proc/cpuinfo"))
           (mhz-lines (remove-if-not (lambda (s) (u:string-prefix-p "cpu MHz" s)) lines)))
      (* (/ (u:sum (mapcar (lambda (l)
                             (u:~> (u:split-sequence #\: l :remove-empty-subseqs t :from-end t)
                                   u:lastcar
                                   u:trim-whitespace
                                   u:parse-float))
                           mhz-lines))
            ncpus)
         1000000)))

  (defun si-frequency (freq-hz)
    (u:format-file-size-human-readable nil freq-hz :suffix "Hz" :flavor :si :space t))

  (let ((last-usage ""))
    (defun modeline-cpu (modeline)
      (declare (ignore modeline))
      (u:fmt "CPU: ~,2f% ~a"
             (u:if-let ((new-usage (cpu-usage)))
               (setf last-usage new-usage)
               last-usage)
             (si-frequency (round (cpu-frequency-hz)))))))
(add-screen-mode-line-formatter #\C 'modeline-cpu)

(defun modeline ()
  (with-output-to-string (s)
    (princ "^4[^B%n^b]^* ^2%W^" s) ;; Window group and windows on the left
    (princ ">" s) ;; Move to right
    (princ "%M | " s) ;; Memory
    (princ "%D | " s) ;; Disk
    (princ "%N | " s) ;; Net
    (princ "%C | " s) ;; CPU
    (princ "^4^B%d" s) ;; Time
    ))

(setf *window-format* "%m%n%s%c"
      *time-modeline-string* "%a %b %e %k:%M"
      *mode-line-timeout* 10
      *mode-line-pad-x* 0
      *mode-line-pad-y* 0
      *mode-line-border-width* 0
      *mode-line-position* :bottom
      *mode-line-background-color* "#000000"
      *mode-line-foreground-color* "#bbbbbb"
      *mode-line-border-color* *mode-line-background-color*
      *screen-mode-line-format* (modeline))

(enable-mode-line (current-screen) (current-head) t)
