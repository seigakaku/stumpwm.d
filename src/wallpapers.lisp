(in-package #:stumpwm-user)

(defparameter *wallpaper-directory* (uiop:xdg-data-home "wallpapers/"))
(defparameter *wallpaper-interval* 60)
(defvar *wallpaper-timer* nil)
(defvar *current-wallpapers* (make-hash-table :test 'equalp :size 3))

(defun fseek (stream n)
  (assert (file-position stream (+ (file-position stream) n))))

(defmethod big-endian->integer ((buf sequence) n-bytes &optional (start 0))
  (reduce (u:op (+ (ash _ 8) _)) buf :start start :end (+ start n-bytes) :initial-value 0))

(defmethod big-endian->integer ((stream stream) n-bytes &optional (start 0))
  (unless (fseek stream start)
    (loop repeat start do (read-byte stream)))
  (reduce (lambda (acc x)
            (+ (ash acc 8) (read-byte stream)))
          (u:iota (1+ n-bytes))))

;;;; ========================================================

(u:define-constant +png-header+
    #(#x89 #x50 #x4e #x47 #x0d #x0a #x1a #x0a)
  :test #'equalp)

(defmethod image-size ((type (eql :png)) file)
  (let ((file (uiop:ensure-pathname file :want-file t))
        (header (make-array 8 :element-type '(unsigned-byte 8))))
    (with-open-file (stream file :direction :input :element-type '(unsigned-byte 8))
      (read-sequence header stream)
      (assert (equalp header +png-header+))
      (fseek stream 8)
      (list (big-endian->integer stream 4)
            (big-endian->integer stream 4)))))

;;;; ========================================================

(u:define-constant +jpg-header+
    #(#xff #xd8)
  :test #'equalp)

(defun jpg-find-sof (stream)
  (loop
    for b = (read-byte stream)
    do (when (= #xFF b)
         (let ((next (read-byte stream)))
           (cond
             ((or (= next #x00) (= next #x01) (<= #xd1 next #xd9))  nil)
             ((or (= next #xc0) (= next #xc2)) (return))
             (t (fseek stream (- (big-endian->integer stream 2) 2))))))))

(defun jpg-read-size-from-sof (stream)
  (fseek stream 3)
  (let ((height (big-endian->integer stream 2))
        (width (big-endian->integer stream 2)))
    (list width height)))

(defmethod image-size ((type (eql :jpg)) file)
  (let ((file (uiop:ensure-pathname file :want-file t))
        (header (make-array 2 :element-type '(unsigned-byte 8))))
    (with-open-file (stream file :direction :input :element-type '(unsigned-byte 8))
      (read-sequence header stream)
      (assert (equalp header +jpg-header+))
      (jpg-find-sof stream)
      (jpg-read-size-from-sof stream))))

;;;; ========================================================

(defun byte-compare (bytes-a bytes-b &key (count (length bytes-b)))
  (loop
    for i below count
    unless (= (aref bytes-a i) (aref bytes-b i))
      return nil
    finally (return t)))

(defun identify-image-type (file)
  (with-open-file (stream file :direction :input :element-type '(unsigned-byte 8))
    (let ((start (make-array 32 :element-type '(unsigned-byte 8))))
      (read-sequence start stream)
      (cond
        ((byte-compare start +png-header+) :png)
        ((byte-compare start #(#xFF #xD8)) :jpg)
        (t nil)))))

(defmethod image-size ((type null) file)
  (let ((type (identify-image-type file)))
    (unless type
      (error "Couldn't determine image type of ~a" file))
    (image-size type file)))

;;;; ========================================================

(defconstant +maximum-aspect-ratio-distance+ 0.01)

(u:defstruct-read-only
    (monitor
     (:copier nil)
     (:predicate nil)
     (:constructor make-monitor (name width height)))
  (name :type simple-string)
  (height :type fixnum)
  (width :type fixnum))

(defun parse-monitors ()
  (assert (u:resolve-executable "xrandr"))
  (loop
    for monitor in (cdr (uiop:run-program "xrandr --listactivemonitors" :output :lines))
    for (nil nil size name) = (u:split-sequence-if #'u:whitespacep (u:collapse-whitespace (u:trim-whitespace monitor)))
    for sizestr = (car (uiop:split-string size :max 3 :separator '(#\x #\+)))
    for (width nil height nil) = (uiop:split-string size :max 4 :separator '(#\/ #\x))
    collect (make-monitor name (parse-integer width) (parse-integer height))))

(defun image-suitable-for-monitor-p (monitor file)
  (with-accessors ((mw monitor-width) (mh monitor-height)) monitor
    (destructuring-bind (iw ih) (image-size nil file)
      (let ((ir (/ iw ih)) (mr (/ mw mh)))
        (and (>= iw mw) (>= ih mh) (<= (abs (- ir mr)) +maximum-aspect-ratio-distance+))))))

(defun suitable-files-for-monitor (monitor directory &optional (recurse? t))
  (let* ((directory (uiop:ensure-pathname directory :ensure-directory t :want-non-wild t))
         (files (uiop:directory-files directory (if recurse? uiop:*wild-path* uiop:*wild-file-for-directory*))))
    (remove-if (u:op (not (image-suitable-for-monitor-p monitor _))) files)))

(defun pick-random (pool excluded &key (test #'uiop:pathname-equal))
  (loop
    for choice = (u:random-elt pool)
    while (member choice excluded :test test)
    finally (return choice)))

(defun pick-wallpapers (directory &key (different? t) (new? t))
  (let ((picked nil) (errors nil))
    (dolist (monitor (parse-monitors))
      (let* ((name (monitor-name monitor))
             (current (gethash name  *current-wallpapers*))
             (pool (suitable-files-for-monitor monitor directory)))
        (if pool
            (let ((excluded (cons (when new? current) (when different? (mapcar #'car picked)))))
              (if (set-difference pool excluded :test #'uiop:pathname-equal)
                  (setf picked (acons monitor (pick-random pool excluded) picked))
                  (push (u:fmt "No possible wallpapers found for output ~a with current constraints" name) errors)))
            (push (u:fmt "No suitable wallpapers found for output ~a" name) errors))))
    (when errors
      (message "~{~a~^~%~}" errors))
    picked))

(defun set-wallpaper (output file)
  (uiop:run-program (u:fmt "xwallpaper --output '~a' --focus '~a'" output file))
  (setf (gethash output *current-wallpapers*) file))

;;;; ========================================================

(defcommand next-wallpapers () ()
  (loop
    for (monitor . file) in (pick-wallpapers *wallpaper-directory*)
    do (set-wallpaper (monitor-name monitor) file)))

(defcommand start-wallpaper-cycler () ()
  (unless *wallpaper-timer*
    (setf *wallpaper-timer* (run-with-timer *wallpaper-interval* *wallpaper-interval* #'next-wallpapers))))

(defcommand stop-wallpaper-cycler () ()
  (when *wallpaper-timer*
    (cancel-timer *wallpaper-timer*)
    (setf *wallpaper-timer* nil)))

(run-commands "next-wallpapers" "start-wallpaper-cycler")
