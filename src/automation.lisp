(in-package #:stumpwm-user)

(when (xlib:query-extension *display* "XTEST")
  (pushnew :clx-ext-test *features*))

#+clx-ext-test
(progn
  (defun ratclick (button)
    "Use XTEST to fake a press of BUTTON"
    (xlib/xtest:fake-button-event *display* button t)
    (xlib/xtest:fake-button-event *display* button nil))

  (defun ratwarp (x y)
    "Use XTEST to move the pointer to [X,Y]"
    (xlib/xtest:fake-motion-event *display* x y :delay 0))

  (defun ratkey (code)
    "Use XTEST to fake a key press of the key
 represented by CODE (X11 Keycode)"
    (xlib/xtest:grab-control *display* t)
    (xlib/xtest:fake-key-event *display* code t)
    (xlib/xtest:fake-key-event *display* code nil)
    (xlib/xtest:grab-control *display* nil))

  (defun ratsync ()
    "Sync the X11 display"
    (xlib:display-finish-output *display*)))

(defun window-size (win)
  (list (window-width win) (window-height win)))

(defun window-position (win)
  (let ((frame (slot-value win 'stumpwm::frame)))
    (list (+ (frame-x frame) (window-x win))
          (+ (frame-y frame) (window-y win)))))

(defun window-geometry (win)
  (nconc (window-position win) (window-size win)))

(defun pointer-position ()
  (xlib:query-pointer (screen-root (current-screen))))

(defun window-at-point ()
  "Get the window where point is currently above, if no windows are under point,
returns NIL, meaning the root window."
  (multiple-value-bind (px py) (pointer-position)
    (loop
      for win in (group-windows (current-group))
      for (x y w h) = (window-geometry win)
      when (and (>= px x) (< px (+ x w))
                (>= py y) (< py (+ y h)))
        return win)))
