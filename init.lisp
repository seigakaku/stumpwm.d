(in-package #:stumpwm-user)

(uiop:add-package-local-nickname :u :serapeum/bundle :stumpwm-user)

(setf *random-state* (make-random-state t))

(when (u:resolve-executable "emacs")
  (uiop:launch-program "emacs --daemon"))

(setf *colors* '("#140A1D" ;; Black
                 "#b52a5b" ;; Red
                 "#ff4971" ;; Green
                 "#8897f4" ;; Yellow
                 "#bd93f9" ;; Blue
                 "#e9729d" ;; Magenta
                 "#f18fb0" ;; Cyan
                 "#f1c4e0")) ;; White
(update-color-map (current-screen))

(psetf *mouse-focus-policy* :click
       ;; Windows
       *maxsize-border-width* 0
       *transient-border-width* 0
       *normal-border-width* 0
       *window-border-style* :none
       *ignore-wm-inc-hints* t
       ;; Input and message windows
       *message-window-gravity* :top-right
       *message-window-input-gravity* :top-right
       *input-window-gravity* :top-right

       *rotation-focus-policy* :master-or-follow)

(set-dynamic-group-initial-values
 :head-placement-policy :current-frame
 :master-layout :left
 :default-split-ratio 2/3)

(loop for i from 1 to 9 do (gnew (u:fmt "~d" i)))
(gselect "default")
(gkill)
(gselect "1")

(defun hostp (host)
  (string-equal (uiop:hostname) (string host)))

(defmacro switch-hosts (&body body)
  (u:with-gensyms (hostname)
    `(let ((,hostname (uiop:hostname)))
       (cond
         ,@(mapcar (lambda (clause)
                     (destructuring-bind (hosts &rest body) clause
                       (if (member hosts '(otherwise t))
                           `(t ,@body)
                           (let ((hosts (u:ensure-list hosts)))
                             `((member ,hostname ',hosts :test #'string=) ,@body)))))
                   body)))))

(defmacro define-stumpwm-enum-type (name keywords)
  `(define-stumpwm-type ,name (input prompt)
     (u:make-keyword
      (string-upcase
       (argument-pop-or-read input prompt (mapcar #'string-downcase ',keywords))))))

(defmacro define-keys (map &body keydefs)
  (u:once-only (map)
    `(progn
       ,@(loop for (k v) on keydefs by #'cddr
               collect (let ((k (if (listp k)
                                    `(apply #'mkksym ,k)
                                    `(kbd ,k))))
                         `(define-key ,map ,k ,v))))))

(defmacro define-prefix-map (map prefix &body keydefs)
  (u:once-only (map prefix)
    (u:with-gensyms (pmap m)
      `(progn
         (defvar ,pmap
           (let ((,m (make-sparse-keymap)))
             (define-keys ,m ,@keydefs)
             ,m))
         (define-key ,map (kbd ,prefix) ',pmap)))))


(defun swm-path (path)
  (uiop:merge-pathnames* path (uiop:merge-pathnames* #P".stumpwm.d/" (user-homedir-pathname))))

(handler-case
    (progn
      (load (swm-path "src/x11.lisp"))
      (load (swm-path "src/modeline.lisp"))
      (load (swm-path "src/wallpapers.lisp"))
      (load (swm-path "src/brightness.lisp"))
      (load (swm-path "src/audio.lisp"))
      (load (swm-path "src/bindings.lisp")))
  (asdf:compile-file-error (e) (declare (ignore e))
    (load (swm-path "dump-image.lisp"))
    (stumpwm:restart-hard)))

;; This must go after loading everything, so that we avoid that we can dump the new executable
;; with a single thread
(u:nlet rec ((port 4005))
  (handler-case
      (slynk:create-server :port port :dont-close t)
    (sb-bsd-sockets:address-in-use-error (e) (declare (ignore e))
      (rec (1+ port)))))
